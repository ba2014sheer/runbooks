---

image: registry.gitlab.com/gitlab-com/runbooks:3.0.2

stages:
  - images
  - test
  - deploy
  - alertmanager
  - deploy-rules
  - deploy-rules-production

.deploy-rules:
  extends: .rules-artifacts
  stage: deploy-rules
  script:
    - gcloud auth activate-service-account --key-file ${SERVICE_KEY}
    - gcloud config set project ${PROJECT}
    - gcloud container clusters get-credentials ${CLUSTER} --region ${REGION}
    - kubectl apply --namespace monitoring --filename ${CI_PROJECT_DIR}/rules-k8s/
  only:
    refs:
      - master
    variables:
      - $CI_API_V4_URL == "https://ops.gitlab.net/api/v4"

.rules-artifacts:
  artifacts:
    expire_in: 1 day
    paths:
      - rules-k8s

.dashboards:
  image: registry.gitlab.com/gitlab-com/runbooks/runtools_build:latest
  before_script:
    - dashboards/bundler.sh # Install jsonnet bundles
    - dashboards/generate-mixins.sh # Generate dashboards from mixins
  only:
    variables:
      - $CI_PROJECT_URL =~ /^https:\/\/gitlab\.com\/.*/

verify:
  stage: test
  image: registry.gitlab.com/gitlab-com/runbooks/runtools_build:latest
  script:
    - make verify

test-rules:
  extends: .rules-artifacts
  stage: test
  script:
    - gem install bundler --no-document
    - bundle
    - ./bin/create_kubernetes_rules.rb --create --validate

rubocop:
  stage: test
  script:
    - gem install bundler etc json --no-document
    - bundle
    - rubocop ./bin/create_kubernetes_rules.rb ./lib/* ./spec/*

rspec:
  stage: test
  script:
    - gem install bundler --no-document
    - bundle
    - bundle exec rspec

deploy-rules-gstg:
  environment: gstg
  extends: .deploy-rules
  stage: deploy-rules

deploy-rules-pre:
  environment: pre
  extends: .deploy-rules
  stage: deploy-rules

deploy-rules-production:
  environment: gprd
  extends: .deploy-rules
  stage: deploy-rules-production

update-alert-manager:
  stage: alertmanager
  script:
    - cd alertmanager
    - ./template.rb
    - ./update.sh
  only:
    refs:
      - master
    changes:
      - alertmanager/*
    variables:
      - $CI_API_V4_URL == "https://ops.gitlab.net/api/v4"

test:
  stage: test
  script:
    - scripts/validate-service-mappings
    - find . -name \*.y*ml | xargs yaml-lint
    - /prometheus/promtool check rules rules/*.yml
    # Prometheus config checks are stricter than rules checks, so use a fake config to check this too
    - /prometheus/promtool check config scripts/prometheus.yml
    - scripts/validate_kibana_urls
    - scripts/validate-alerts

update_elastic_dashboards:
  stage: deploy
  image: registry.gitlab.com/gitlab-com/runbooks/runtools_build:latest
  script:
  - ./elastic/managed-objects/dashboards/update-dashboards.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/dashboards/*
    variables:
      - $ES_URL

update_elastic_index_patterns:
  stage: deploy
  image: registry.gitlab.com/gitlab-com/runbooks/runtools_build:latest
  script:
  - ./elastic/managed-objects/index-patterns/update-index-patterns.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/index-patterns/*
    variables:
      - $ES_URL

update_elastic_searches:
  stage: deploy
  image: registry.gitlab.com/gitlab-com/runbooks/runtools_build:latest
  script:
  - ./elastic/managed-objects/searches/update-searches.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/searches/*
    variables:
      - $ES_URL

update_elastic_watchers:
  stage: deploy
  image: registry.gitlab.com/gitlab-com/runbooks/runtools_build:latest
  script:
  - ./elastic/managed-objects/watches/update-watches.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/watches/*
    variables:
      - $ES_URL

update_elastic_visualizations:
  stage: deploy
  image: registry.gitlab.com/gitlab-com/runbooks/runtools_build:latest
  script:
  - ./elastic/managed-objects/visualizations/update-visualizations.sh
  only:
    refs:
      - master
    changes:
      - elastic/managed-objects/visualizations/*
    variables:
      - $ES_URL

dryrun_pingdom_checks:
  stage: test
  image: golang:1.11
  script:
    - cd pingdom
    - go run pingdom.go --dry-run
  except:
    refs:
      - master
  only:
    variables:
      - $CI_PROJECT_URL =~ /^https:\/\/gitlab\.com\/.*/

deploy_pingdom_checks:
  stage: deploy
  image: golang:1.11
  script:
    - cd pingdom
    - go run pingdom.go
  only:
    refs:
      - master
    variables:
      - $CI_PROJECT_URL =~ /^https:\/\/gitlab\.com\/.*/

check_alerts:
  image: golang:1.12
  script:
    - cd alerts-checker
    - go run alerts-checker.go ../rules $THANOS_URL $IGNORED_ALERTS
  only:
    variables:
      - $PERFORM_ALERTS_CHECK

test_dashboards:
  extends: .dashboards
  stage: test
  script:
    - dashboards/upload.sh -D

deploy_dashboards:
  extends: .dashboards
  stage: deploy
  script:
    - dashboards/upload.sh
  only:
    refs:
      - master

build_runtools_build_image:
  stage: images
  image: docker:stable
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
  script:
    - apk add curl git
    - git clone https://github.com/google/jsonnet.git
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - cd jsonnet && docker build -t registry.gitlab.com/gitlab-com/runbooks/runtools_build-intermediate:latest . && cd ..
    - docker build -t registry.gitlab.com/gitlab-com/runbooks/runtools_build:latest images/runtools_build/
    - docker push registry.gitlab.com/gitlab-com/runbooks/runtools_build:latest
  when: manual
